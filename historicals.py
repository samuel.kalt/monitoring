from __future__ import absolute_import, division
import time
import datetime
import numpy as np
import pandas as pd


class Historicals:
    def __init__(self, file_path_historicals):
        # instruments
        self.instrument_prices = None
        self.instrument_positions = None

        self.instrument_ideal_positions = None
        self.instrument_position_pnls = None
        self.instrument_trading_pnls = None
        self.instrument_total_pnls = None

        # factors
        self.factor_prices = None
        self.factor_positions = None
        self.factor_emas = None
        self.factor_ideal_positions = None
        self.factor_position_pnls = None
        self.factor_trading_pnls = None
        self.factor_total_pnls = None

    def update(self):
        # adda row everywhere
        pass

    def recompute_factor_price(self, model):
        data = model.instrument_to_factor_price(self.instrument_prices)
        self.factor_prices = pd.DataFrame(
            columns=model.factors, index=self.instrument_prices.index, data=data,
        )

    def recompute_factor_emas(self, model):
        data = model.instrument_to_factor_price(self.instrument_prices)
        self.factor_prices = pd.DataFrame(
            columns=model.factors, index=self.instrument_prices.index, data=data,
        )

    def update_historical_emas(self):
        factors = historical_factors.columns
        historical_emas = pd.DataFrame(index=historical_factors.index, columns=factors)
        for factor in factors:
            ema_length = factors[factor].ema_length
            ema_length_adjusted_for_historicals_update_interval = (
                ema_length / historicals_update_interval
            )
            alpha = 1 - np.exp(
                -1 / ema_length_adjusted_for_historicals_update_interval
            )  # todo this must match the index, adjustment?
            historical_emas[factor] = historical_factors[factor].ewm(alpha=alpha).mean()
        return historical_emas
