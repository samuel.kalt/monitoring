import pandas as pd
import os
from tabulate import tabulate
from model import Model
from historicals import Historicals

pd.options.display.float_format = "{:,.3f}".format

file_path_model = r"/mnt/c/Users/samuel.kalt/Desktop/monitoring/model.xlsx"
file_path_short_term = (
    r"/mnt/c/Users/samuel.kalt/Desktop/monitoring/short_term_prices.xlsx"
)
file_path_long_term = (
    r"/mnt/c/Users/samuel.kalt/Desktop/monitoring/long_term_prices.xlsx"
)


class Monitor:
    def __init__(self):
        # fixed inputs
        self.settings = None
        self.model = Model(file_path_model)

        # connections
        self.connection = None

        # live values
        self.prices = None

        # historicals
        self.short_term = Historicals(file_path_short_term)
        self.long_term = Historicals(file_path_long_term)

    # continuous monitoring
    def pull_data(self):
        pass

    def update_live(self):
        pass

    def update_short_term(self):
        pass

    def update_long_term(self):
        pass

    def run_loop(self):
        pass

    # generate reports
    def daily_report(self):
        pass

    def weekly_report(self):
        pass

    def monthly_report(self):
        pass


if __name__ == "__main__":

    monitor = Monitor()
    hist = pd.read_excel(file_path_long_term, index_col=0)
    x = monitor.model.instrument_to_factor_price(hist)
    df = pd.DataFrame(index=hist.index, columns=monitor.model.factors, data=x)
    print(x)
