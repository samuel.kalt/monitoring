from __future__ import absolute_import, division
import pandas as pd
import numpy as np
import os
from tabulate import tabulate


pd.options.display.float_format = "{:,.3f}".format


class Model:
    def __init__(self, file_path_model):

        self.pricing = pd.read_excel(file_path_model, sheet_name="pricing", index_col=0)
        self.factors = list(self.pricing.columns.values)
        self.outrights = list(self.pricing.index.values)

        self.risk_with_spreads = pd.read_excel(
            file_path_model, sheet_name="risk", index_col=0
        )

        self.risk = self.risk_with_spreads[: len(self.outrights)]

        self.dollarized_pricing = pd.read_excel(
            file_path_model, sheet_name="dollarized_pricing", index_col=0
        )

        self.inverse_dollarized_pricing_with_spreads = pd.read_excel(
            file_path_model, sheet_name="inverse_dollarized_pricing", index_col=0
        )
        self.inverse_dollarized_pricing = self.inverse_dollarized_pricing_with_spreads[
            : len(self.outrights)
        ]

        # transpose
        self.pricing_transpose = self.pricing.T
        self.risk_with_spreads_transpose = self.risk_with_spreads.T
        self.risk_transpose = self.risk.T
        self.dollarized_pricing_transpose = self.dollarized_pricing.T
        self.inverse_dollarized_pricing_with_spreads_transpose = (
            self.inverse_dollarized_pricing_with_spreads.T
        )
        self.inverse_dollarized_pricing_transpose = self.inverse_dollarized_pricing.T

    # current values
    def instrument_to_factor_position(self, instrument_position):
        return np.dot(instrument_position, self.risk)

    def instrument_to_factor_price(self, instrument_price):
        return np.dot(instrument_price, self.dollarized_pricing)

    def factor_to_instrument_position(self, factor_position):
        return np.dot(factor_position, self.pricing_transpose)

    def factor_to_instrument_price(self, factor_price):
        return np.dot(factor_price, self.inverse_dollarized_pricing_transpose)

    # historical values # todo is this the same as the single instrument function?
    def historical_instrument_to_factor_price(self, historical_instrument_price):
        return np.dot(historical_instrument_price, self.dollarized_pricing)


if __name__ == "__main__":
    file_path_model = r"/mnt/c/Users/samuel.kalt/Desktop/monitoring/model.xlsx"
    model = Model(file_path_model)
    print(model.risk)
    print(
        model.factor_to_instrument_position([0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    )

